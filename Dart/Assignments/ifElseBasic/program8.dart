// Divisible by three and five

void main() {
	int no = 15;
	
	if(no%3==0 && no%5==0) {
		print("Number is divisible by both 3 and 5");
	}else if(no%3==0) {
		print("Number is divisible by 3 Only");
	}else if(no%5==0) {
		print("Number is divisible by 5 Only");
	}else {
		print("Number is not divisible by 3 or 5");
	}
}
