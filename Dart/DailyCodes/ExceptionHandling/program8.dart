// throw keyword in dart
// throwing predefined exception

import 'dart:io';

int calcProf(int sale , int pur) {
	if(sale<pur)
		throw FormatException;
	else
		return sale-pur;
}

void main() {
	print("Start main");
	
	int? prof;

	try {
		prof = calcProf(1500,1600);
	}catch(ex) {
		print("exception handled");
	}
	print("End main");
}
