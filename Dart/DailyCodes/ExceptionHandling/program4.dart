// NullPointerException

class Demo {
	Demo() {
		print("In constructor");
	}

	void fun() {
		print("In fun demo");
	}
}

void main() {
	print("Start main");
	
	Demo? obj = Demo();

	obj?.fun();

	obj = null;

	obj?.fun();
}
