// User defined exception

class MyException implements Exception{
	String? msg;

	MyException(this.msg);

	String toString()=>msg!;
}

void main() {
	print("Start main");

	try {
		throw new MyException("This is my exception");
	}on MyException{
		print("ex.toString()");
	}
	print("End main");
}
