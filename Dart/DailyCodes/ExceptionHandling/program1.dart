// Exception handling in dart
// Exception is the aborting behaviour of the program when met to a certain 
// invalid inputs and conditions . Exceptions breaks the normal flow of the 
// program to abnormal termination

import 'dart:io';

void main() {

	print("Enter a number to divide with 5");
	int? x = int.parse(stdin.readLineSync()!);

	print(5/x);	// if x is 0 there is no exception in dart it is infinity

	print("End main");
}
