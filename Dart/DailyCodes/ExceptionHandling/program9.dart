// User defined exception

class MyException {
	String? msg;

	MyException(this.msg);

	String toString()=>msg!;
}

void main() {
	print("Start main");

	try {
		throw new MyException("This is my exception");
	}on MyException{
		print("ex.toString()");
	}
	print("End main");
}
