// Exception handling in dart

import 'dart:io';

void main() {

	print("Start main");
	
	try {
		print("Enter number : ");
		int x = int.parse(stdin.readLineSync()!);
		
		print(x);
	}catch(ex) {	// no need to specify the name of the class
		print("Exception caught");
		print(ex);
	}

	print("Endmain");
}
