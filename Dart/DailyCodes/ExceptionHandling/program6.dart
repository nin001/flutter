// On keyword in exception handling
// if we know the exact name of the exception that is going to be thrown
// then we use on with the exception

import 'dart:io';

void main() {
	
	print("Start main");
	
	try {
		int x = int.parse(stdin.readLineSync()!);
	
		print(x);
	}on /*FormatException*/IntegerDivisionByZeroException{
		print("format exception handled");
	}catch(ex) {
		print("Kuthe nahi gela tr ithe yenar");
	}

	print("End main");
}
