// Future class is used in dart for async programming
// Future.delayed is the constructor which is used to delay a task/function

void fun() {
	print("In fun");
}

void main() {
	print("Statement1");
	print("Statement2");

	Future.delayed(Duration(seconds:5),()=>fun());

	print("Statement3");
	print("End main");
}

// fun() call gets delayed and the rest gets executed 
// When we use delayed constructor , the event fun() gets delayed for 5 seconds
// and rest events from event queue is excecuted by the event loop in isolate

