// Similar delayed method in java is Thread.sleed()

class ThreadDemo {
	public static void main(String args[]) {
		System.out.println("Statement1");
		System.out.println("Statement2");
		try {
		Thread.sleep(5000);
		}catch(InterruptedException ie) {}
		System.out.println("Statement3");
		System.out.println("End main");
	}
}

// Statements after Thread.sleep() waits for 5 seconds till execution
// reason of this is because of the Thread (main thread) goes to sleep 
// for 5 seconds and the program here is single threaded and hence it waits
// for 5 seconds and then rest code is executed
