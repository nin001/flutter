// async and await in asyncprogramming

// async and await are used to wait for the function on which some Future constructor 
// is applied

Future<String> fun1() {
	return Future.delayed(Duration(seconds:5),()=>"Hello");
}

Future<void> main() async{
	print("Start main");
	print(await fun1());
	print("End main");
}
