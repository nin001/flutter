// Sir given code

void fun2() {
	for(int i = 0 ; i<10 ; i++) {
		print("In fun2");
	}
	Future.delayed(Duration(seconds:1),()=>print("Delayed"));
	for(int i = 0 ; i<1000 ; i++) {
		print("Delayed fun2");
	}
}

void fun1() {
	for(int i = 0 ; i<1000 ; i++) {
		print("In fun1");
	}
}

void main() {
	print("Start main");
	fun2();
	fun1();
	print("End main");
}
