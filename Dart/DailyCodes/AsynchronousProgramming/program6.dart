// input code
import 'dart:io';

String? placeOrder() {
	print("Enter Your order :");
	return stdin.readLineSync();
}

Future<String?> getOrder() {
	print("Hault");
	return Future.delayed(Duration(seconds:5),()=>placeOrder());
}

Future<String> getOrderMsg() async{
	var order = await getOrder();
	return "Your order is $order";
}

Future<void> main() async{
	print("Start Order");
	print(await getOrderMsg());
	print("End Order");
}
