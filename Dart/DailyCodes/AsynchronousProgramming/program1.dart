// Synchronous programming
// Execution of tasks in a sequential manner where next task is 
// executed only when the previous task is completely executed

void fun() {
	print("In fun");
}

void main() {

	print("Statement1");
	print("Statement2");

	fun();
	
	print("Statement3");
}
