// File class has also provided methods that we do not need to use 
// Async programming and internally dart handles the asynchronous programming
// behaviour and we get programming

import 'dart:io';

void main() {
	File f = new File("ninSync.txt");

	f.createSync();		// We here dont need to use await and async and file gets created
	
	print(f.lastModifiedSync());
	print(f.lastAccessedSync());
	print(f.existsSync());

}
