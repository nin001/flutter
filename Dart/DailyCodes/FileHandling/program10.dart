// Deleting directory using dart

import 'dart:io';

void main() {
	Directory d = new Directory("trash");
	
	print(d.path);
	print(d.absolute);
	
	d.delete(recursive:true);
}
