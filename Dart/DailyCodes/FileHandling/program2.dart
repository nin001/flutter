// The tasks where there is communication between the program
// and the hardware we should use the functions using async programming
// This will make sure that the values returned is complete and
// output wont be a 'instance of ' value that is object

import 'dart:io';

Future<void> main() async {
	File f = new File("nin.txt");

	print(f.runtimeType);

	await f.create();	// return value Future<File>
	
	print("File created");
}
