// Reading from a file 
// Methods : readAsString and readAsStringSync

import 'dart:io';
import 'dart:async';

Future<void> main() async {
	
	File f = new File("ninSync.txt");

	String data = await f.readAsString();
	
	//Future<String> content = await f.readAsStringSync();
	//content.then((val)=>print(val));

	print(data);
}
