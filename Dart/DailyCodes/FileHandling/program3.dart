// Methods in File class

import 'dart:io';

void main() {
	File f = new File("c2w.txt");
	
	print(f.absolute);
	print(f.lastModified());
	print(f.lastAccessed());
	print(f.path);
	print(f.length());
	print(f.exists());

}

/*
File: '/mnt/d/CODES/flutter/Dart/DailyCodes/FileHandling/c2w.txt'
Instance of 'Future<DateTime>'
Instance of 'Future<DateTime>'
c2w.txt
Instance of 'Future<int>'
Instance of 'Future<bool>'
*/
