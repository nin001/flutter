// Deleting file using dart
// method : deleteSync

import 'dart:io';

void main() {
	File f = new File("deleteDemo.txt");

	if(f.existsSync()) {
		f.deleteSync();
		print("Deletion completed");
	}else {
		print("File doesnt exist");
	}
}
