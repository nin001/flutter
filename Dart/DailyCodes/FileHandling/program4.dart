// As we are dealing with files that are present in the hardware
// we need to use async programming inorder to get the value of the file
// properly

import 'dart:io';

Future<void> main() async{

	File f = new File("nin.txt");

	print(await f.lastModified());
	print(await f.lastAccessed());
	print(await f.exists());
}

/*
2023-12-16 15:13:45.000
2023-12-16 15:13:45.000
true
*/
