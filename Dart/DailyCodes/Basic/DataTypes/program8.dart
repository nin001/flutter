/* Assigning different data type value

when we create a variable of var type and initialize it
with a data type , and then after initializing if we try
to assign value of different type then it will show error

var is a generic data type , but it becomes the data type 
which it is initialized
*/

void main() {
	var x = "Niraj";
	print(x);

	x = 19;  // inserting int value in var type x;
	print(x);
}
/*
Error
A value of type 'int' type cant be assigned in 
'String'
*/
