/* int 
// int is a class in dart whose parent is num class , int 
can encorporate integer value only

int cannot be left null , we need to assign value to it before use

*/

void main() {
	int x;
	print(x);	// Error : Non-nullable variable 'x' must be assigned before 
			// can be used

}
