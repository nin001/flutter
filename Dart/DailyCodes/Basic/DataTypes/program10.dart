// const keyword , 

// Const keyword is used to make the value of the variable 
// constant and we cannot assign or change the value of the variable

void main() {
	const int x = 10;

	const y = 20;
	// we can write const variable without actually giving its type 
	// dart recognizes the variable as per the data 
	
	print(y.runtimeType);
	
	//x = 100;	
	//y = 200;	
	// ERROR:
	//  cannot assign value to a constant variable
	
	const int z;	// If we write a variable in dart without initialization
        // dart uses null safety and gives error that z should be initialized

}
