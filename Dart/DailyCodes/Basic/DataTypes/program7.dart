/*
var
var is a data type which can encorporate any type of 
data and changes var to the encorporated data type
*/

void main() {
	var x = "Shashi";
	print(x);
	
	var y = 20.5;
	print(y);
}
