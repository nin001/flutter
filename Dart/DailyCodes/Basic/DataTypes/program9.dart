
// Dynamic data type in Dart

// Dynamic is a data type , which doesnt bind the data type with
// the variable , that is it changes variable data type as per the
// value

void main() {
	dynamic x = "Niraj";
	print(x.runtimeType);	//String

	x = 20.5;
	print(x.runtimeType);	//double
	
	x = 100;
	print(x.runtimeType);	// int
}
