/*
Assigning double value to the int type

In java , Possible loosy conversions error would occur
in compile time 
*/

void main() {
	int x = 10;
	print(x);
	num y = 50;
	print(y);
	
	x = 35.5;	// compile time error of 
			// A value of 'double' type cant be assigned to a variable 	
			// of type 'int'
	print(x);
}
