/*
Tring assigning int value in double type

In java , int value was converted to double due to no data
loss and also , double size is larger than int
*/

void main() {
	double x = 10.5;
	print(x);

	num y = 20;
	print(y);
	y = 20.35;

	x = 11;
	print(x);
}
