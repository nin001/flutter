/*
Data types in dart
There are several data types in dart
in Basic part we can explore data types like
- String 
- Number
- Boolean

Data types in dart is class , everything in dart programming
is of object type
*/

/* num - a class which is paernt class in the number type , this class
 can encorporate integer type and as well as double type that is decimal values
 This class is the parent of int and double*/

void main() {
	int x = 10;
	print(x);
}
