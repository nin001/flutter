// Object 
// it is parent class of all classes in dart programming
// We can use Object to encorporate every type of data

void main() {
	Object x = 10;
	print(x);
	print(x.runtimeType);
	
	x = "Niraj";
	print(x);
	print(x.runtimeType);

	x = 20.25;
	print(x);
	print(x.runtimeType);
}
