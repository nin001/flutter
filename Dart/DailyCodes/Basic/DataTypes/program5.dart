/*
String is a class in dart programming language
S in String is capital in dart because in other
OOP languages the S is capital and other primitive data 
type like int , double , bool are small , This is followed by
dart programming language
*/

void main() {
	String name1 = "Core2Web";
	String name2 = 'Incubator';

	print(name1);
	print(name2);
}

