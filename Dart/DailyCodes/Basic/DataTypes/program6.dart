/*
Boolean data type in dart
'bool' a data type which can hold two data true and false
*/

void main() {
  bool flag = true;
  print(flag);

  flag = false;
  print(flag);
}
