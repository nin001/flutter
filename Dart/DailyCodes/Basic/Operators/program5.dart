// Logical operator

// ! - not
// && - Logical and
// || - logical or

void main() {
	int x = 10;
	int y = 8;

	print(x && y);
	print(x || y);
	print(!x);
	print(!y);
}
// We can only use bool values while using logical operators

/*
Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(x && y);
              ^
program5.dart:11:13: Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(x && y);
                   ^
program5.dart:12:8: Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(x || y);
              ^
program5.dart:12:13: Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(x || y);
                   ^
program5.dart:13:9: Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(!x);
               ^
program5.dart:14:9: Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
        print(!y);
               ^*/
