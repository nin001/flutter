/*Operators in dart
	3Types
----------------------------
|	     |	            |
Unary	  Binary	Ternary
*/

// Unary operator

// pre and post

void main() {
	int x = 5;
	print(++x);
	print(x++);
	print(x);
}
