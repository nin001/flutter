// Constructor is a Speacial type of function which is called implicitly while creating object
// Constructor is used to initialize instance variables 
// Name of Constructor and class is same

class Demo {
	String? name;
	int? age;

	Demo(String name , int age) {
		print("In Demo Constructor");
		this.name = name;
		this.age;
	}

	// Other way
	/*Demo(this.name,this.age);	// this initializes the instance variables and if doesnt given body 
					// it works fine
	*/

}

void main() {
	Demo obj = new Demo("niraj",19);
}
