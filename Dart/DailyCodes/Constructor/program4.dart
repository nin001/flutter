// Optional parameter in Constructor is Marked under the list
// "[]" and Default parameter is Marked in "{}"

class Education {
	String? college;
	String? uni;

	Education(this.college,{this.uni = "Pune University"});		//Default parameter

	void info() {
		print("College name : $college , University : $uni");
	}
}

void main() {
	Education edu1 = new Education("SCOE");
	edu1.info();

	//Education edu2 = new Education("MIT","Mumbai University");
	//edu2.info();		error : too many positional arguments 
}
