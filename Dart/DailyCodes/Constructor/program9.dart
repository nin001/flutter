// Named constructor

// as Constructor cannot have same name , to write multiple constructors in 
// dart named constructor is used
// Syntax : <ClassName>.somename


class Demo {
	int? x = 10;

	Demo() {
		print("Default constructor");
	}

	Demo.init(this.x);

	void printX() {
		print(x);
	}
}

void main() {
	Demo x = new Demo();

	Demo y = new Demo.init(20);

	y.printX();
}
