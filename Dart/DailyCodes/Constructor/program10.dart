// Constant consturctor

// To init in constant constructor , variables should be declared final

class Demo {
	final String? str;
	final int? x;

	const Demo(this.str,this.x);
}

void main() {
	Demo obj = new Demo("string",10);
}
