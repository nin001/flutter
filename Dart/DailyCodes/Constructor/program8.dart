// In dart , Constructor is also a object
// This is the reason that multiple constructor with same name
// is not supported

class Demo {
	int? x;

	Demo() {
		print("Default constructor");
	}

	Demo(this.x) {
		print("Parameterized construtor");
	}
}
