// Ways to write constructor
// this.variables as a parameter

class Company {
	String? companyName;
	String? location;

	Company(this.companyName,this.location) {
		print("In constructor");
	}

	void compInfo() {
		print("company $companyName : location $location");
	} 
}

void main() {
	Company obj = new Company("Veritas","Baner");

	obj.compInfo();
}
