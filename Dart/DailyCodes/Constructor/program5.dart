// Named paramter constructor
// in named paramter construtor while creating the object we need to specify the name
// of the variables used and assign the values accordingly

class Vehicle {
	String? name;
	String? type;

	Vehicle({this.name,this.type});

	void info() {
		print("Vehicle : $name | Type : $type");
	}
}

void main() {
	
	//Vehicle bike = new Vehicle("Pulser","bike"); error

	Vehicle bike = new Vehicle(name:"Pulser",type:"bike");
	
	bike.info();

	Vehicle car = new Vehicle(type:"car",name:"Swift");

	car.info();
}
