// Difference between Final and Constant

// Constant :
	// needs to be initialized
	// if given value cannot be changed

// final:
	// no need to initialize
	// runtime value can be assigned but once assigned cannot changed

import 'dart:io';

void main() {
	//const int? x;
	
	const int y = 10;

	print(y);
	//y = 10;
	
	final int? a;
	a = int.parse(stdin.readLineSync()!);
	
	print(a);

	//a = 20;
}
