// Types of constructor in dart

// Default
class Demo {
	Demo() {
		print("Default constructor");	// no paramters 
	}
}

void main() {
	Demo obj = new Demo();
}
