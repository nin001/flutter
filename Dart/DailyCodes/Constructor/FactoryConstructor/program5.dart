import "program4.dart";

void main() {
	Backend js = new Backend("javascript");
	print(js.lang);

	Backend java = new Backend("java");
	print(java.lang);

	Backend py = new Backend("python");
	print(py.lang);
}
