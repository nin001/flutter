
class Demo {
	Demo._private() {
		print("In constructor");
	}
	
	factory Demo() {
		return Demo._private();
	}
}
