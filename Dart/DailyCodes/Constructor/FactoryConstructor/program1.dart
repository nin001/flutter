// Factory constructor in Dart
// Factory constructor is a speacial type of constructor that is not same as generative constructors
// Factory constructor can return a object of the class or subclass or even a cached instance 

// cannot use this keyword in factory cosntructor
// must return an instance of the class or the subclass
// It can be named or unnamed and is called like normal constructors
// It cannot access instance members of the class

class Demo {

	static Demo obj = new Demo();

	Demo() {
		print("In Demo Constructor");
	}
	
	Demo fun() {
		return obj;
	}
}

void main() {
	Demo obj = new Demo();
	
}
