
class Backend {
	String? lang;
	
	Backend._private(String lang) {
		if(lang == "javascript") this.lang = "NodeJS";
		else if(lang == "java") this.lang = "Springboot";
		else this.lang = "NodeJS/Springboot";
	}
	
	factory Backend(String lang) {
		print("Factory");
		return Backend._private(lang);
	}
}
