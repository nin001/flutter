// return child class 

abstract class Developer {
	factory Developer(String role) {
		if(role == "frontend") return Node();
		else if(role == "backend") return Spring();
		else return Other();
	}
}

class Node implements Developer {
	Node() {
		print("In Node Constructor");
	}
}

class Spring implements Developer {
	Spring() {
		print("In Spring Constructor");
	}
}

class Other implements Developer {
	Other() {
		print("In Other Constructor");
	}
}


void main() {
	Developer obj1 = new Developer("frontend");
	Developer obj2 = new Developer("backend");
	Developer obj3 = new Developer("Testing");
}
