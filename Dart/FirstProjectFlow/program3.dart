// Const constructor and final instance variables

//Const constructor uses : if same with same arguments objects are created
//		instead of creating multiple objects it returns same object
//		while creating object we should const 

class Demo {
	final String str;
	final int x;

	const Demo(this.str,this.x);

	void fun() {
		print("In fun");
		print(x);
		print(str);
	}
}

void main() {

	Demo obj = Demo("Niraj",19);
	obj.fun();
}
