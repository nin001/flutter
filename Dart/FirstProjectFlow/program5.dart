// Inheritance

class Parent {

	void data() {
		print("in data");
	}
	void marry() {
		print("arrange");
	}
}

class Child extends Parent {
	
	void marry() {
		print("AM");
	}
}

void main() {
	Child obj = Child();
	
	obj.marry();
	obj.data();
}
