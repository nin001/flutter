// Function returning object

class Demo {
	Demo() {
		print("In Demo Constructor");
	}

	void fun() {
		print("In fun method");
	}
}

Demo objFun() {
	print("In objFun");
	return Demo();
}

void main() {

	objFun().fun();
}
