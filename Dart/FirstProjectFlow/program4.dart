// Const and final 

class Demo {
	final String str;
	final int x;

	const Demo(this.str,this.x);

	void fun() {
		print("In fun method");
		print(x);
		print(str);
	}
}

Demo objFun() {
	print("In objfun");
	return const Demo("Niraj",19);
}

void main() {
	Demo obj = objFun();
	Demo obj2 = objFun();

	print(obj.hashCode);
	print(obj2.hashCode);
}
