// Passing object to function

class Demo {
	Demo() {
		print("In demo constructor");
	}
	
	void fun() {
		print("In Fun Method");
	}
}

void objFun(Demo obj) {
	print("In obj fun");
	obj.fun();
}

void main() {
	Demo obj = Demo();

	objFun(obj);
}
